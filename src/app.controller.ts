import {Body, Controller, Post, Request, UseGuards} from '@nestjs/common';
import {ValidationPipe} from './shared/pipes/validation.pipe';
import {UserDTO} from './users/user.dto';
import {LocalAuthGuard} from './auth/local-auth.guard';
import {AuthService} from './auth/auth.service';

@Controller()
export class AppController {

    constructor(private authService: AuthService) {
    }

    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Body(new ValidationPipe()) dto: UserDTO, @Request() req) {
        return this.authService.login(req.user);
    }
}
