import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {UsersModule} from '../users/users.module';
import {PassportModule} from '@nestjs/passport';
import {LocalStrategy} from './local.strategy';
import {JwtModule} from '@nestjs/jwt';
import {configService} from '../config/config.service';
import {JwtStrategy} from './jwt.strategy';

@Module({
    exports: [
        AuthService,
    ],
    imports: [
        UsersModule,
        PassportModule,
        JwtModule.register({
            secret: configService.getJwtSecret(),
            signOptions: {expiresIn: '12h'},
        }),
    ],
    providers: [
        AuthService,
        LocalStrategy,
        JwtStrategy,
    ]
})
export class AuthModule {
}
