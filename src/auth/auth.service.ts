import {Injectable} from '@nestjs/common';
import {UsersService} from '../users/users.service';
import {User} from '../model/user.entity';
import {JwtService} from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService,
                private jwtService: JwtService) {
    }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne(username);

        if (user) {
            const passwordOk = await bcrypt.compare(pass, user.password);
            if (passwordOk) {
                const {password, ...result} = user;
                return result;
            }
        }
        return null;
    }

    async login(user: User) {
        const payload = {username: user.username, sub: user.id, roles: ['ADMIN']};
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
