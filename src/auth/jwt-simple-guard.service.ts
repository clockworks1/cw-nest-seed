import {ForbiddenException, Injectable, UnauthorizedException} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';

@Injectable()
export class JwtSimpleGuard extends AuthGuard('jwt') {
    handleRequest(err: any, user: any, info: any) {
        return user;
    }
}
