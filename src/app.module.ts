import {HttpModule, Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {configService} from "./config/config.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {UtilsService} from './shared/services/utils.service';
import {MulterModule} from '@nestjs/platform-express';
import {AuthModule} from './auth/auth.module';
import {UsersModule} from './users/users.module';
import {SettingController} from './endpoints/settings/setting.controller';
import {SettingService} from './endpoints/settings/setting.service';
import {Setting} from './model/setting.entity';

@Module({
    imports: [
        TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
        TypeOrmModule.forFeature([
            Setting,
        ]),
        MulterModule.register({
            dest: './files',
        }),
        AuthModule,
        UsersModule,
        HttpModule,
    ],
    controllers: [
        AppController,
        SettingController,
    ],
    providers: [
        AppService,
        UtilsService,
        SettingService,
    ],
})
export class AppModule {
}
