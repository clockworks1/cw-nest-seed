import {Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Request, UseGuards} from '@nestjs/common';
import {SettingDto} from "./setting.dto";
import {SettingService} from "./setting.service";
import {ValidationPipe} from "../../shared/pipes/validation.pipe";
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {JwtAuthGuard} from '../../auth/jwt-auth-guard.service';
import {JwtSimpleGuard} from '../../auth/jwt-simple-guard.service';

@ApiTags('Settings')
@ApiBearerAuth()
@Controller('settings')
export class SettingController {

    constructor(private settingService: SettingService) {
    }

    @Get()
    @UseGuards(JwtSimpleGuard)
    public async getAll(@Request() req) {
        return await this.settingService.getAll(req.user && req.user.roles.includes('ADMIN'));
    }

    @Post()
    @UseGuards(JwtAuthGuard)
    public async post(@Body(new ValidationPipe()) dto: SettingDto): Promise<SettingDto> {
        const path = SettingDto.from(dto)
        return this.settingService.create(path);
    }

    @Put(':id')
    @UseGuards(JwtAuthGuard)
    public async update(@Param('id') id: string, @Body(new ValidationPipe()) dto: SettingDto) {
        const setting = SettingDto.from(dto)
        return this.settingService.update(id, setting);
    }

    @Delete(':id')
    @UseGuards(JwtAuthGuard)
    @HttpCode(204)
    public async remove(@Param('id') id: string) {
        return this.settingService.delete(id);
    }
}
