import {Injectable} from '@nestjs/common';
import {SettingDto} from './setting.dto';
import {Setting} from '../../model/setting.entity';

@Injectable()
export class SettingService {

    constructor() {
    }

    public async getAll(withPrivate?: boolean) {
        let query = Setting.createQueryBuilder('settings');

        if (!withPrivate) {
            query = query.where('settings.private IS FALSE');
        }

        return query.getMany();
    }

    public async create(dto: SettingDto): Promise<SettingDto> {
        return Setting.save(dto.toEntity())
            .then(e => SettingDto.fromEntity(e));
    }

    public async get(id: string) {
        return Setting.findOne(id);
    }

    public async update(id: string, dto: SettingDto): Promise<SettingDto> {
        return Setting.save(dto.toEntity())
            .then(e => SettingDto.fromEntity(e));
    }

    public async delete(id: string) {
        const setting = await Setting.findOne(id);
        if (setting) {
            await Setting.softRemove(setting);
        }
        return;
    }
}
