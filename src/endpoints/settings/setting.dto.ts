import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import {IsBoolean, IsNotEmpty, IsOptional, IsString, IsUUID} from "class-validator";
import {Setting} from '../../model/setting.entity';


export class SettingDto implements Readonly<SettingDto> {
    @ApiModelProperty()
    @IsUUID()
    @IsOptional()
    id: string;

    @ApiModelProperty({required: true})
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiModelProperty()
    @IsString()
    @IsOptional()
    value: string;

    @ApiModelProperty()
    @IsBoolean()
    @IsOptional()
    private: boolean;

    public static from(dto: Partial<SettingDto>) {
        const it = new SettingDto();
        it.id = dto.id;
        it.name = dto.name;
        it.value = dto.value;
        it.private = dto.private;

        return it;
    }

    public static fromEntity(entity: Setting) {
        return this.from({
            id: entity.id,
            name: entity.name,
            value: entity.value,
            private: entity.private,
        });
    }

    public toEntity() {
        const it = new Setting();
        it.id = this.id;
        it.name = this.name;
        it.value = this.value;
        it.private = this.private;

        return it;
    }
}
