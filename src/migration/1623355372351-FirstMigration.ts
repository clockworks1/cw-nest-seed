import {MigrationInterface, QueryRunner} from "typeorm";

export class FirstMigration1623355372351 implements MigrationInterface {
    name = 'FirstMigration1623355372351'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "username" character varying(300) NOT NULL, "password" character varying(300) NOT NULL, "email" character varying(300) NOT NULL, "firstName" character varying(300) NOT NULL, "lastName" character varying(300) NOT NULL, CONSTRAINT "UQ_fe0bb3f6520ee0469504521e710" UNIQUE ("username"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`INSERT INTO "users" ("username", "password", "email", "firstName", "lastName") VALUES ('admin', '$2y$10$ql4E7M8OUfBtg/MA7l/89OJ4748SKO41PY3opN2aj.Su55LUtL35i', 'admin@admin.com', 'Admin', 'Admin')`);
        await queryRunner.query(`CREATE TABLE "settings" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP, "name" character varying(300) NOT NULL, "value" character varying(300) NOT NULL, "private" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_0669fe20e252eb692bf4d344975" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "settings"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
