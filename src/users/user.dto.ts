import {ApiModelProperty} from "@nestjs/swagger/dist/decorators/api-model-property.decorator";
import {IsNotEmpty, IsString} from "class-validator";

export class UserDTO implements Readonly<UserDTO> {

    @ApiModelProperty({required: true})
    @IsString()
    @IsNotEmpty()
    username: string;

    @ApiModelProperty({required: true})
    @IsString()
    @IsNotEmpty()
    password: string;

}
