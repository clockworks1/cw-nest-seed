import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {configService} from "./config/config.service";
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import * as bodyParser from 'body-parser';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, {cors: true});
    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

    if (!configService.isProduction()) {

        const document = SwaggerModule.createDocument(app, new DocumentBuilder()
            .setTitle('CW NEST SEED')
            .setDescription('The famous NEST SEED')
            .addBearerAuth()
            .build());

        SwaggerModule.setup('docs', app, document);

    }

    const PORT = Number(process.env.PORT) || 3000;
    await app.listen(PORT);
}

bootstrap();
