import {Injectable} from '@nestjs/common';
import { extname } from 'path';

@Injectable()
export class UtilsService {
    generateCode(size?: number): string {
        if (!size) {
            size = 4;
        }
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const charactersLength = characters.length;
        for (let i = 0; i < size; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}

export const imageFileFilter = (req, file, callback) => {
    if (!file.originalname.toLowerCase().match(/\.(jpg|jpeg|png|gif|svg|webp)$/)) {
        return callback(new Error(`Only image files are allowed! Denying ${file.originalname}.`), false);
    }
    callback(null, true);
};

export const editFileName = (req, file, callback) => {
    const randomName = getRandomFileName(file.originalname)
    callback(null, randomName);
}

export const setFileName = (req, file, callback) => {
    callback(null, file.originalname.replace('#', '-'));
}

export const getRandomFileName = (filename) => {
    const name = filename.split('.')[0];
    const fileExtName = extname(filename.toLowerCase());
    const randomName = Array(4)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    return `${name}-${randomName}${fileExtName}`;
}