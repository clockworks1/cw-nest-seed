import {Injectable, PipeTransform} from "@nestjs/common";

@Injectable()
export class ToBoolPipe implements PipeTransform<any> {
    async transform(value: any) {
        if (typeof value === 'string') {
            switch (value.toLowerCase()) {
                case 'false':
                case 'null':
                case 'undefined':
                    return false;
                case 'true':
                    return true;
                default:
                    return isNaN(+value) ? true : !!+value;
            }
        }
        return !!value;
    }
}
