import {Column, Entity} from "typeorm";
import {CWBaseEntity} from "./cw-base.entity";
import {Expose} from 'class-transformer';

@Entity({name: 'users'})
export class User extends CWBaseEntity {

    @Column({type: 'varchar', length: 300, unique: true})
    username: string;

    @Column({type: 'varchar', length: 300})
    password: string;

    @Column({type: 'varchar', length: 300})
    email: string;

    @Column({type: 'varchar', length: 300})
    firstName: string;

    @Column({type: 'varchar', length: 300})
    lastName: string;

    @Expose()
    get fullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }

}
