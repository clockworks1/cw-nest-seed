import {Column, Entity} from "typeorm";
import {CWBaseEntity} from "./cw-base.entity";

@Entity({name: 'settings'})
export class Setting extends CWBaseEntity {

    @Column({type: 'varchar', length: 300})
    name: string;

    @Column({type: 'varchar', length: 300})
    value: string;

    @Column({type: 'boolean', default: false})
    private: boolean;
}
